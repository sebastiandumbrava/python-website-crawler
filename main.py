from bs4 import BeautifulSoup
import urllib
import urllib.request
import requests
import sys
import re

url = sys.argv[1]

urls_explored = set()
urls_unexplored = set()
urls_params_dict = dict()

def get_url_params(url):

    return

def discover(url):
    header={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}
    req = urllib.request.Request(url, headers=header)
    link = urllib.request.urlopen(req)
    soup = BeautifulSoup(link, features='lxml')
    links = []

    l = soup.find_all('a', attrs={'href': re.compile("^http://")})

    for link in l:
        url = link.get('href')
        links.append(url)
    return links

def crawl():
    while len(urls_unexplored) > 0:
        link = urls_unexplored.pop()
        discovered = discover(link)
        urls_explored.add(link)
        
    for link in discovered:
        if link not in urls_explored:
            print(link)
            urls_unexplored.add(link)

        for link in discovered:
            get_url_params(link)


    return


def main():
    urls_unexplored.add(url)
    crawl()

    for link in urls_explored:
        print(link)

    return

main()
